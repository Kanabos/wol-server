package main

import (
	"encoding/json"
	"github.com/mdlayher/wol"
	"log"
	"net"
	"net/http"
	"os"
)

type WOLPacket struct {
	Mac string `json:"mac"`
}

type WOLServerConfig struct {
	Interface string
	Port      string
}

var ConfigInstance *WOLServerConfig

func sendWOLPacket(packet WOLPacket) bool {
	hwMac, err := net.ParseMAC(packet.Mac)
	if err != nil {
		log.Println("Error parsing mac address")
		return false
	}

	wlanInet, wlanInetErr := net.InterfaceByName(ConfigInstance.Interface)
	if wlanInetErr != nil {
		log.Printf("Error finding interface: %v\n", wlanInetErr)
		return false
	}

	wolClient, wolClientErr := wol.NewRawClient(wlanInet)
	if wolClientErr != nil {
		log.Printf("Error initializing WOL client %v\n", wolClientErr)
		return false
	}

	log.Printf("Sending WOL packet to %v\n", hwMac)
	wolWakeErr := wolClient.Wake(hwMac)
	if wolWakeErr != nil {
		log.Printf("Err %v", wolWakeErr)
		return false
	}

	return true
}

func wolHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
		// Decode the JSON in the body and overwrite 'tom' with it
		decoder := json.NewDecoder(r.Body)
		wolPacket := &WOLPacket{}
		err := decoder.Decode(wolPacket)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		if !sendWOLPacket(*wolPacket) {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		log.Printf("I can't process method %v\n", r.Method)
	}
}

func htmlHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		log.Printf("Serve /index.html")
		http.ServeFile(w, r, "static/index.html")
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		log.Printf("I can't process method %v\n", r.Method)
	}
}

func configureHttpServer() {
	http.HandleFunc("/api/wol", wolHandler)
	http.HandleFunc("/", htmlHandler)
}

func GetEnv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}

func loadConfig() *WOLServerConfig {
	inet := GetEnv("INET", "eth0")
	port := GetEnv("PORT", "9337")

	return &WOLServerConfig{
		inet,
		port,
	}
}

func main() {
	log.Println("==========================")
	log.Println("======= WOL SERVER =======")
	log.Println("==========================")
	log.Println("\n-- v1.0")

	ConfigInstance = loadConfig()
	configureHttpServer()
	err := http.ListenAndServe(":"+ConfigInstance.Port, nil)
	if err != nil {
		log.Printf("Fail starting server. Err %v", err)
		return
	}
}
