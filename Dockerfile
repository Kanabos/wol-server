FROM golang:1.21.4-alpine AS BuidlStage

WORKDIR /wol-server-src
COPY . .

RUN go mod download
RUN go build -o /wol-server main.go

FROM  alpine:latest

WORKDIR /
COPY --from=BuidlStage /wol-server /wol-server
COPY static /static

EXPOSE 9337

CMD ["/wol-server"]
